$version="5.2.0.9"
$package_exist =(choco search jbs --version $version | Select -last 1)
if ($package_exist -eq '0 packages found.') {
	Write-Host "Packing"
	choco pack JBS\jbs.nuspec
	Write-Host "Test installation"
	cup -y jbs -s .\
	Write-Host "Pushing the package"
	choco apiKey -k $choco_api --source https://push.chocolatey.org/
	choco push jbs.$version.nupkg --source https://push.chocolatey.org/
}